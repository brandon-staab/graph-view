add_library(project_packages INTERFACE)


# assimp
find_package(assimp)
target_link_libraries(project_packages INTERFACE assimp)


# Boost
find_package(Boost 1.71.0 REQUIRED COMPONENTS graph)
target_link_libraries(project_packages INTERFACE Boost::graph)


# GLEW
find_package(GLEW REQUIRED)
target_link_libraries(project_packages INTERFACE GLEW::GLEW)


# GLM
find_package(glm REQUIRED)
target_link_libraries(project_packages INTERFACE glm)


# OpenGL
set(OpenGL_GL_PREFERENCE "GLVND")
find_package(OpenGL REQUIRED)
target_link_libraries(project_packages INTERFACE OpenGL::OpenGL)


# SFML
find_package(SFML 2.5
	COMPONENTS
		system
		window
		graphics
	REQUIRED
)
target_link_libraries(project_packages INTERFACE
	sfml-system
	sfml-window
	sfml-graphics
)


# Threads
set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads REQUIRED)
target_link_libraries(project_packages INTERFACE Threads::Threads)
