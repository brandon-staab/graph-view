add_custom_target("run"
    COMMAND app $(args)
    DEPENDS "app"
    WORKING_DIRECTORY "${CMAKE_PROJECT_DIR}"
)

add_custom_target("debug"
    COMMAND gdb --ex=run --cd=$<TARGET_FILE_DIR:app> --args app
    DEPENDS "app"
    WORKING_DIRECTORY "${CMAKE_PROJECT_DIR}"
)

add_custom_target("docs"
    COMMAND $ENV{BROWSER} html/index.html
    DEPENDS "doxygen"
    WORKING_DIRECTORY "${CMAKE_PROJECT_DIR}"
)
