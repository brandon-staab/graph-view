Graph View [![pipeline status](https://gitlab.com/brandon-staab/graph-view/badges/master/pipeline.svg)](https://gitlab.com/brandon-staab/graph-view/commits/master)
===============================

What is this?
-------------------------------
3D Graph viewing software.

### Cube
<img src="./docs/cube.png" alt="Cube" height="200" />

### Sheet
<img src="./docs/sheet.png" alt="Sheet" height="200" />

### Tube
<img src="./docs/tube.png" alt="Tube" height="200" />

### Binary Tree
<img src="./docs/binary_tree.png" alt="Binary Tree" height="200" />

Requirements
-------------------------------
```shell
./dependencies.sh
```

* C++ 20
* OpenGL 4.5

Setting up build environment
-------------------------------
```shell
mkdir -p build
cd build
cmake ..
```

Running
-------------------------------
```shell
make run args="<filename>"
make run args="../assests/graphs/cube.grf"
```

Documentation
-------------------------------
### [README.md](https://gitlab.com/brandon-staab/graph-view/-/blob/master/README.md)

### Doxygen
```shell
make docs
```

Testing
-------------------------------
```shell
make check
```

Debugging
-------------------------------
```shell
make debug
```
