#version 460 core

uniform mat4 projection;
uniform mat4 view;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;
layout (location = 5) in mat4 model;

out vec2 tex_coords_fs;


void main() {
	tex_coords_fs = tex_coords;
	gl_Position = projection * view * model * vec4(position, 1.0);
}

