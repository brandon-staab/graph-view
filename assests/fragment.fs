#version 460 core

uniform sampler2D texture_diffuse1;

in vec2 tex_coords_fs;

out vec4 color;


void main() {
    color = texture(texture_diffuse1, tex_coords_fs);
}

