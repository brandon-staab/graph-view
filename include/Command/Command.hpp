#pragma once

class Client;


class Command {
  public:
	enum class Kind : unsigned char {
		close_cmd,
		resize_cmd,

		reset_cmd,
		origin_cmd,
		pan_cmd,
		pos_fov_cmd,
		neg_fov_cmd,

		left_acceleration_cmd,
		right_acceleration_cmd,
		front_acceleration_cmd,
		back_acceleration_cmd,
		up_acceleration_cmd,
		down_acceleration_cmd,

		COUNT,
	};

	explicit Command(Kind kind);
	Command(Command const& other) = delete;
	Command(Command const&& other) = delete;
	Command&& operator=(Command const& other) = delete;
	Command&& operator=(Command&& other) = delete;
	virtual ~Command() = default;

	[[nodiscard]] Kind get_kind() const;
	virtual void execute(Client* client) const = 0;

  private:
	Kind const m_kind;
};
