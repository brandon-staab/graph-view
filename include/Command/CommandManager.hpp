#pragma once

#include "Command/Command.hpp"

#include <array>
#include <memory>


class CommandManager {
  public:
	CommandManager();

	Command const* get(Command::Kind const kind) const;

  private:
	std::array<std::unique_ptr<Command>, static_cast<std::size_t>(Command::Kind::COUNT)> m_table;
};

