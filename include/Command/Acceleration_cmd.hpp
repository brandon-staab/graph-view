#pragma once

#include "Command/Command.hpp"


class Acceleration_cmd final : public Command {
  public:
	Acceleration_cmd() = delete;
	explicit Acceleration_cmd(Kind kind);

	void execute(Client* client) const final;

  private:
};
