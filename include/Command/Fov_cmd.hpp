#pragma once

#include "Command/Command.hpp"


class Fov_cmd final : public Command {
  public:
	Fov_cmd() = delete;
	Fov_cmd(Kind const kind);

	void execute(Client* client) const final;

  private:
};
