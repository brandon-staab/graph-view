#pragma once

#include "Command/Command.hpp"


class Pan_cmd final : public Command {
  public:
	Pan_cmd();

	void execute(Client* client) const final;

  private:
};
