#pragma once

#include "Command/Command.hpp"


class Close_cmd final : public Command {
  public:
	Close_cmd();

	void execute(Client* client) const final;

  private:
};
