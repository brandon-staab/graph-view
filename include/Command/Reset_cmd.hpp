#pragma once

#include "Command/Command.hpp"


class Reset_cmd final : public Command {
  public:
	Reset_cmd();

	void execute(Client* client) const final;

  private:
};
