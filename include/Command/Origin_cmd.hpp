#pragma once

#include "Command/Command.hpp"


class Origin_cmd final : public Command {
  public:
	Origin_cmd();

	void execute(Client* client) const final;

  private:
};
