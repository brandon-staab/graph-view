#pragma once

#include "Command/Command.hpp"


class Resize_cmd final : public Command {
  public:
	Resize_cmd();

	void execute(Client* client) const final;

  private:
};
