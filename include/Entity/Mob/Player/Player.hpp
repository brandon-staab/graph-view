#pragma once

#include "Entity/Mob/Mob.hpp"
#include "Entity/Mob/Player/PlayerCamera.hpp"


class Player final : public Mob {
  public:
	Player(PhysicsManager* physics_manager) noexcept;
	Player(Player const& other) noexcept = delete;
	Player(Player&& other) noexcept = default;
	Player& operator=(Player const& rhs) noexcept = delete;
	Player& operator=(Player&& rhs) noexcept = default;
	~Player() noexcept final = default;

	void tick() noexcept;
	[[nodiscard]] PlayerCamera const& get_camera() const noexcept;
	PlayerCamera& get_camera() noexcept;

  private:
	PlayerCamera m_camera;
};
