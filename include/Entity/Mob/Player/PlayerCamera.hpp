#pragma once

#include "Physics/AdvancedPhysicsProperties.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>

class Player;
class Renderer;


class PlayerCamera {
  public:
	explicit PlayerCamera(Player const& player) noexcept;

	void update(Renderer* renderer) noexcept;
	[[nodiscard]] glm::mat4 get_view() noexcept;

  private:
	[[nodiscard]] glm::vec3 get_eye() const noexcept;
	[[nodiscard]] glm::vec3 get_center() const noexcept;
	[[nodiscard]] static glm::vec3 get_up() noexcept;
	[[nodiscard]] glm::vec3 get_forward() const noexcept;
	[[nodiscard]] AdvancedPhysicsProperties const& get_properties() const noexcept;

  private:
	Player const& m_player;
};
