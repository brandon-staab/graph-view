#pragma once

#include "Entity/Physics_entity.hpp"


class Mob : public Physics_entity {
  public:
	explicit Mob(Kind kind, PhysicsManager* physics_manager) noexcept;

  private:
};
