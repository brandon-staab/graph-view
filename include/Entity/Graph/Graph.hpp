#pragma once

#include "Entity/Graph/Edge.hpp"
#include "Entity/Graph/Node.hpp"

#include <boost/graph/adjacency_list.hpp>

#include <utility>

class Renderer;

class Graph final : public Physics_entity {
  public:
	using BoostGraph = boost::adjacency_list<boost::setS, boost::vecS, boost::undirectedS, Node, Edge>;
	using EdgeIters = std::pair<BoostGraph::edge_iterator, BoostGraph::edge_iterator>;
	using VertexIters = std::pair<BoostGraph::vertex_iterator, BoostGraph::vertex_iterator>;

	explicit Graph(PhysicsManager* physics_manager, std::string const& path);

	void tick(Renderer* renderer) noexcept;

	[[nodiscard]] BoostGraph const& get_boost_graph() const noexcept;
	[[nodiscard]] BoostGraph& get_boost_graph() noexcept;
	[[nodiscard]] EdgeIters get_edges() const noexcept;
	[[nodiscard]] EdgeIters get_edges() noexcept;
	[[nodiscard]] VertexIters get_nodes() const noexcept;
	[[nodiscard]] VertexIters get_nodes() noexcept;

	void update_edge_positions() noexcept;
	void update_node_positions() noexcept;

  private:
	unsigned m_iterations;
	float m_delta;
	BoostGraph m_boost_graph;
};
