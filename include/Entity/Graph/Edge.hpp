#pragma once

#include "Entity/Physics_entity.hpp"


class Edge final : public Physics_entity {
  public:
	Edge() noexcept;  // Dummy contrustor for boost
	Edge(PhysicsManager* physics_manager, glm::vec3 start, glm::vec3 end) noexcept;

	[[nodiscard]] glm::mat4 get_matrix() const noexcept final;
	void set_location(glm::vec3 start, glm::vec3 end) noexcept;

  private:
};
