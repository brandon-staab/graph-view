#pragma once

#include "Entity/Physics_entity.hpp"


class Node final : public Physics_entity {
  public:
	Node() noexcept;  // Dummy contrustor for boost
	explicit Node(PhysicsManager* physics_manager, glm::vec3 pos) noexcept;

	[[nodiscard]] glm::mat4 get_matrix() const noexcept final;

	void set_pos(glm::vec3 pos) noexcept;
	[[nodiscard]] glm::vec3 get_pos() const noexcept;

	void set_velocity(glm::vec3 delta) noexcept;
	void add_velocity(glm::vec3 delta) noexcept;
	[[nodiscard]] glm::vec3 get_velocity() const noexcept;

  private:
};
