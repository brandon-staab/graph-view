#pragma once

#include <glm/glm.hpp>


class Entity {
  public:
	enum class Kind : unsigned char {
		graph_entity,
		node_entity,
		edge_entity,
		player_entity,
	};

	explicit Entity(Kind kind) noexcept;
	Entity(Entity const& other) noexcept = default;
	Entity(Entity&& other) noexcept = default;
	Entity& operator=(Entity const& rhs) noexcept = delete;
	Entity& operator=(Entity&& rhs) noexcept = default;
	virtual ~Entity() noexcept = default;

	[[nodiscard]] Kind get_kind() const noexcept;
	[[nodiscard]] virtual glm::mat4 get_matrix() const noexcept;

  private:
	Kind m_kind;
};
