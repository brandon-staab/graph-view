#pragma once

#include "Entity/Entity.hpp"
#include "Physics/PhysicsProperties.hpp"

class PhysicsManager;

class Physics_entity : public Entity {
  public:
	explicit Physics_entity(Kind kind, PhysicsManager* physics_manager) noexcept;

	[[nodiscard]] PhysicsProperties::Kind get_physics_kind() const noexcept;
	[[nodiscard]] PhysicsProperties const* get_physics() const noexcept;
	[[nodiscard]] PhysicsProperties* get_physics() noexcept;

  private:
	PhysicsProperties* const m_physics;
};
