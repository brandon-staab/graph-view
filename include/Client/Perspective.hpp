#pragma once

#include "Client/Renderer/Renderer.hpp"

#include <SFML/Window/Window.hpp>


class Perspective {
  public:
	Perspective(sf::Window& window, Renderer& renderer);
	Perspective(Perspective const& other) = delete;

	void update() const;
	void reset();

	float get_fov() const;
	void set_fov(float const fov);
	void add_fov(float const delta);

  private:
	float m_fov;
	sf::Window& m_window;
	Renderer& m_renderer;
};
