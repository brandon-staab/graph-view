#pragma once

#include "Client/Renderer/ModelManager.hpp"

class Client;

class Renderer {
  public:
	explicit Renderer(Client& client) noexcept;
	~Renderer() = default;

	Renderer(Renderer const& other) = delete;
	Renderer(Renderer&& other) = delete;
	Renderer& operator=(Renderer const& other) = delete;
	Renderer& operator=(Renderer&& other) = delete;

	Client const& get_client() const;
	Client& get_client();
	Shader const& get_shader() const;

	void setup();
	void draw() const;

	Model const* get_model(ModelManager::ModelKind kind) const;
	Model* get_model(ModelManager::ModelKind kind);

	void schedule_update() const;
	void update_matrices() const;

  private:
	Client& m_client;
	Shader m_shader;
	ModelManager m_model_manager;
};
