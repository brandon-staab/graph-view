#pragma once

#include "Client/Renderer/Model.hpp"

#include <array>


class ModelManager {
  public:
	enum class ModelKind : unsigned char {
		edge_model,
		node_model,

		COUNT,
	};

	explicit ModelManager() noexcept = default;

	void setup(std::string const& dir);
	Model const* get_model(ModelKind kind) const;
	Model* get_model(ModelKind kind);

  private:
	void load_model(ModelKind kind, std::string const& path);

  private:
	std::array<Model, static_cast<std::size_t>(ModelKind::COUNT)> m_models;
};
