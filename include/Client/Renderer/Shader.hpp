#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <string>


class Shader {
  public:
	Shader() = default;
	Shader(std::string const& vertex_path, std::string const& fragment_path, std::string const& geometry_path = "");

	void use() const;
	GLuint get_id() const;
	GLuint get_uniform_id(std::string const& name) const;

	void set_bool(std::string const& name, bool const value) const;
	void set_int(std::string const& name, int const value) const;
	void set_float(std::string const& name, float const value) const;

	void set_vec2(std::string const& name, glm::vec2 const& vec) const;
	void set_vec3(std::string const& name, glm::vec3 const& vec) const;
	void set_vec4(std::string const& name, glm::vec4 const& vec) const;

	void set_mat2(std::string const& name, glm::mat2 const& mat) const;
	void set_mat3(std::string const& name, glm::mat3 const& mat) const;
	void set_mat4(std::string const& name, glm::mat4 const& mat) const;

  private:
	GLuint m_id;
};

