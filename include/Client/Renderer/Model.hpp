#pragma once

#include "Client/Renderer/Mesh.hpp"

#include <assimp/scene.h>

using Meshes = std::vector<Mesh>;
using Matrices = std::vector<glm::mat4>;


class Model {
  public:
	Model() = default;
	explicit Model(std::string const& path);
	void draw(Shader const& shader) const;

	Meshes const& get_meshes() const;
	Meshes& get_meshes();

	bool needs_updated() const;
	void schedule_update(bool flag = true) const;
	Matrices const& get_matrices() const;
	Matrices& get_matrices();
	void add_matrix(glm::mat4 const& matrix) const;
	void clear_matrices() const;

	std::string const& get_path() const;
	std::string get_directory() const;
	std::string get_filename() const;

  private:
	void send_matrices() const;
	void setup_matrices();

	void load_model(std::string const& path);
	void process_node(aiNode const* node, aiScene const* scene);
	Mesh process_mesh(aiMesh const* mesh, aiScene const* scene);

	Textures& get_loaded_textures();
	Textures load_material_textures(aiMaterial const* mat, aiTextureType type, std::string const& type_name);

  private:
	bool mutable m_matrices_needs_updated{};
	GLuint m_mat_buffer{};
	std::string m_path;
	Textures m_textures_loaded;
	Meshes m_meshes;
	Matrices mutable m_matrices;
};
