#pragma once

#include "Client/Renderer/Shader.hpp"
#include "Client/Renderer/Texture.hpp"
#include "Client/Renderer/Vertex.hpp"

#include <vector>

using Vertices = std::vector<Vertex>;
using Indices = std::vector<GLuint>;
using Textures = std::vector<Texture>;


class Mesh {
  public:
	Mesh(Vertices const& vertices, Indices const& indices, Textures const& textures);

	void draw(Shader const& shader, std::size_t const amount) const;

	GLuint get_VAO() const;
	Vertices get_vertices() const;
	Indices get_indices() const;
	Textures get_textures() const;

  private:
	void setup_mesh();

  private:
	GLuint m_VAO;
	GLuint m_VBO;
	GLuint m_EBO;

	Vertices m_vertices;
	Indices m_indices;
	Textures m_textures;
};

