#pragma once

#include <span>

#include "Client/Perspective.hpp"
#include "Client/Renderer/Renderer.hpp"
#include "Entity/Graph/Graph.hpp"
#include "Entity/Mob/Player/Player.hpp"
#include "EventHandler/EventHandler.hpp"
#include "Physics/PhysicsManager.hpp"

#include <SFML/Window.hpp>


class Client {
  public:
	explicit Client(std::span<char const* const> args) noexcept(false);

	void run() noexcept(false);
	void tick_physics();
	void handle_events();
	void draw();

	[[nodiscard]] Graph const& get_graph() const noexcept;
	[[nodiscard]] Graph& get_graph() noexcept;

	[[nodiscard]] sf::Window& get_window();
	void close();
	[[nodiscard]] Renderer& get_renderer();

	[[nodiscard]] EventHandler& get_event_handler();
	void exec_cmd(Command::Kind cmd_kind);

	[[nodiscard]] PhysicsManager& get_physics_manager();
	[[nodiscard]] Player& get_player();

	[[nodiscard]] PlayerCamera& get_camera();
	void update_camera();

	[[nodiscard]] Perspective& get_perspective();
	void update_perspective();

  private:
	PhysicsManager m_physics_manager;
	Player m_player;
	std::unique_ptr<Graph> m_graph;
	sf::Window m_window;
	Renderer m_renderer;
	Perspective m_perspective{m_window, m_renderer};
	EventHandler m_event_handler;
};
