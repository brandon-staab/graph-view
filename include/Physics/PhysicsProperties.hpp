#pragma once

#include <glm/glm.hpp>


class PhysicsProperties {
  public:
	enum Kind : unsigned char {
		props,
		basic_props,
		adv_props,
	};

	PhysicsProperties(Kind const kind = Kind::props);
	PhysicsProperties(PhysicsProperties const& other) = delete;
	virtual ~PhysicsProperties() = default;

	Kind get_kind() const;
	virtual void tick_physics();

	glm::vec3 get_pos() const;
	void set_pos(glm::vec3 const pos);
	void add_pos(glm::vec3 const pos);

	glm::vec3 get_facing() const;
	void set_facing(glm::vec3 const facing);
	void spin_facing(glm::vec3 const spin);

	glm::vec3 get_forward() const;
	glm::vec3 get_up() const;
	glm::vec3 get_right() const;

  private:
	Kind const m_kind;
	glm::vec3 m_pos;
	glm::vec3 m_facing;
};

