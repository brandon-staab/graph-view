#pragma once

#include "Physics/BasicPhysicsProperties.hpp"


class AdvancedPhysicsProperties final : public BasicPhysicsProperties {
  public:
	AdvancedPhysicsProperties();

	void tick_physics() final;

	glm::vec3 get_acceleration() const;
	glm::vec3 get_abs_acceleration() const;
	void set_acceleration(glm::vec3 const acceleration);
	void add_acceleration(glm::vec3 const acceleration);
	void scale_acceleration(float const factor);
	void reduce_acceleration();

	glm::vec3 get_torque() const;
	void set_torque(glm::vec3 const torque);
	void add_torque(glm::vec3 const torque);
	void scale_torque(float const factor);
	void reduce_torque();

  private:
	glm::vec3 m_acceleration;
	glm::vec3 m_torque;
};

