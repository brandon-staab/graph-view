#pragma once

#include "Physics/PhysicsProperties.hpp"


class BasicPhysicsProperties : public PhysicsProperties {
  public:
	BasicPhysicsProperties(Kind const kind = Kind::basic_props);

	void tick_physics() override;

	glm::vec3 get_velocity() const;
	void set_velocity(glm::vec3 const velocity);
	void add_velocity(glm::vec3 const velocity);
	void add_velocity_relative(glm::vec3 const velocity);
	void scale_velocity(float const factor);
	void reduce_velocity();

	glm::vec3 get_spin() const;
	void set_spin(glm::vec3 const spin);
	void add_spin(glm::vec3 const spin);
	void scale_spin(float const factor);
	void reduce_spin();

  private:
	glm::vec3 m_velocity;
	glm::vec3 m_spin;
};

