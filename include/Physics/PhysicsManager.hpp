#pragma once

#include "Entity/Entity.hpp"
#include "Physics/PhysicsProperties.hpp"

#include <memory>
#include <vector>


class PhysicsManager {
  public:
	PhysicsManager() = default;
	PhysicsManager(PhysicsManager const& other) = delete;

	void tick();
	PhysicsProperties* create(Entity::Kind const kind);
	std::vector<std::unique_ptr<PhysicsProperties>>& get_objects();

  private:
	std::vector<std::unique_ptr<PhysicsProperties>> m_objects;
};

