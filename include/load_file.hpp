#pragma once

#include <sstream>
#include <string>


std::stringstream load_file(std::string const& path);

