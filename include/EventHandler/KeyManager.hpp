#pragma once

#include "Command/Command.hpp"

#include <SFML/Window/Keyboard.hpp>

#include <array>


class KeyManager {
  public:
	KeyManager();
	KeyManager(KeyManager const& other) = delete;

	Command const* get_pressed_bind(sf::Keyboard::Key const key) const;
	Command const* get_released_bind(sf::Keyboard::Key const key) const;
	void set_key_pressed_bind(sf::Keyboard::Key const key, Command const* const cmd);
	void set_key_released_bind(sf::Keyboard::Key const key, Command const* const cmd);

  private:
	std::array<Command const*, 2 * sf::Keyboard::Key::KeyCount> m_table;
};

