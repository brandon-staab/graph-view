#pragma once

#include "Command/CommandManager.hpp"
#include "EventHandler/KeyManager.hpp"

#include <SFML/Window/Event.hpp>


class EventHandler {
  public:
	EventHandler();
	EventHandler(EventHandler const& other) = delete;

	Command const* get_cmd(Command::Kind const cmd_kind) const;
	Command const* handle(sf::Event const& event) const;

  private:
	Command const* handle_key_pressed(sf::Event::KeyEvent const& key_event) const;
	Command const* handle_key_released(sf::Event::KeyEvent const& key_event) const;
	void set_key_pressed_bind(sf::Keyboard::Key const key, Command::Kind const cmd_kind);
	void set_key_released_bind(sf::Keyboard::Key const key, Command::Kind const cmd_kind);

  private:
	CommandManager m_cmd_mgr;
	KeyManager m_key_mgr;
};

