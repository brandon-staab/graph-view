set(
	CORE_SOURCE

	"src/Client/Client.cpp"
	"src/Client/Perspective.cpp"
	"src/Client/Renderer/Renderer.cpp"
	"src/Client/Renderer/ModelManager.cpp"
	"src/Client/Renderer/Mesh.cpp"
	"src/Client/Renderer/Model.cpp"
	"src/Client/Renderer/Shader.cpp"

	"src/Command/Command.cpp"
	"src/Command/CommandManager.cpp"
	"src/Command/Close_cmd.cpp"
	"src/Command/Acceleration_cmd.cpp"
	"src/Command/Fov_cmd.cpp"
	"src/Command/Origin_cmd.cpp"
	"src/Command/Pan_cmd.cpp"
	"src/Command/Reset_cmd.cpp"
	"src/Command/Resize_cmd.cpp"

	"src/Entity/Entity.cpp"
	"src/Entity/Physics_entity.cpp"
	"src/Entity/Graph/Edge.cpp"
	"src/Entity/Graph/Graph.cpp"
	"src/Entity/Graph/Graph.update_node_positions.cpp"
	"src/Entity/Graph/Node.cpp"
	"src/Entity/Mob/Mob.cpp"
	"src/Entity/Mob/Player/Player.cpp"
	"src/Entity/Mob/Player/PlayerCamera.cpp"

	"src/EventHandler/EventHandler.cpp"
	"src/EventHandler/KeyManager.cpp"

	"src/Physics/PhysicsManager.cpp"
	"src/Physics/PhysicsProperties.cpp"
	"src/Physics/BasicPhysicsProperties.cpp"
	"src/Physics/AdvancedPhysicsProperties.cpp"

	"src/load_file.cpp"
)
