#include "load_file.hpp"

#include <fstream>
#include <iostream>


std::stringstream
load_file(std::string const& path) {
	auto constexpr clean = [](auto& ss) {
		ss.str("");
		ss.clear();
	};

	auto ss = std::stringstream();
	auto ifs = std::ifstream(path.c_str());
	ifs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	if (!ifs.is_open()) {
		clean(ss);
		ss << "File \"" << path << "\" could not be opened.";
		throw std::runtime_error(ss.str());
	}

	try {
		ss << ifs.rdbuf();
	} catch (std::ifstream::failure& e) {
		clean(ss);
		ss << "Failed to load file \"" << path << "\".";
		throw std::runtime_error(ss.str());
	}

	std::clog << "Loader: \"" << path << "\"\n";

	return ss;
}

