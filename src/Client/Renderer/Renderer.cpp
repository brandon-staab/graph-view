#include "Client/Renderer/Renderer.hpp"

#include "Client/Client.hpp"


namespace {
	auto const assests_dir = std::string("assests/");
}  // namespace


Renderer::Renderer(Client& client) noexcept : m_client(client), m_shader() {}


Client const&
Renderer::get_client() const {
	return m_client;
}


Client&
Renderer::get_client() {
	return m_client;
}


Shader const&
Renderer::get_shader() const {
	return m_shader;
}


void
Renderer::setup() {
	glEnable(GL_DEPTH_TEST);

	m_model_manager.setup(assests_dir);
	m_shader = Shader(assests_dir + "vertex.vs", assests_dir + "fragment.fs");
	get_shader().use();

	get_client().update_camera();
	get_client().update_perspective();

	schedule_update();
}


void
Renderer::draw() const {
	update_matrices();

	glClearColor(0.2F, 0.2F, 0.2F, 1.0F);                // NOLINT
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	get_shader().use();
	get_model(ModelManager::ModelKind::edge_model)->draw(get_shader());
	get_model(ModelManager::ModelKind::node_model)->draw(get_shader());
}


Model const*
Renderer::get_model(ModelManager::ModelKind const kind) const {
	return m_model_manager.get_model(kind);
}


Model*
Renderer::get_model(ModelManager::ModelKind const kind) {
	return m_model_manager.get_model(kind);
}


void
Renderer::schedule_update() const {
	get_model(ModelManager::ModelKind::edge_model)->schedule_update();
	get_model(ModelManager::ModelKind::node_model)->schedule_update();
}


void
Renderer::update_matrices() const {
	auto const& graph = get_client().get_graph();

	auto const update_model = [&](auto const& iters, ModelManager::ModelKind const kind) {
		auto const* model = get_model(kind);
		if (model->needs_updated()) {
			model->clear_matrices();

			for (auto [iter, end] = iters; iter != end; iter++) {
				auto const& entity = graph.get_boost_graph()[*iter];
				model->add_matrix(entity.get_matrix());
			}
		}
	};

	update_model(graph.get_edges(), ModelManager::ModelKind::edge_model);
	update_model(graph.get_nodes(), ModelManager::ModelKind::node_model);
}
