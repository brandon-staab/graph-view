#include "Client/Renderer/Mesh.hpp"


Mesh::Mesh(Vertices const& vertices, Indices const& indices, Textures const& textures) : m_VAO(), m_VBO(), m_EBO() {
	m_vertices = vertices;
	m_indices = indices;
	m_textures = textures;

	setup_mesh();
}


void
Mesh::draw(Shader const& shader, std::size_t const amount) const {
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;
	unsigned int heightNr = 1;

	for (auto i = 0U; i < m_textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);

		std::string number;
		std::string name = m_textures[i].type;

		if (name == "texture_diffuse") {
			number = std::to_string(diffuseNr++);
		} else if (name == "texture_specular") {
			number = std::to_string(specularNr++);
		} else if (name == "texture_normal") {
			number = std::to_string(normalNr++);
		} else if (name == "texture_height") {
			number = std::to_string(heightNr++);
		}

		// set the sampler to the correct texture unit
		glUniform1i(glGetUniformLocation(shader.get_id(), (name + number).c_str()), static_cast<GLint>(i));
		glBindTexture(GL_TEXTURE_2D, m_textures[i].id);
	}

	glBindVertexArray(get_VAO());

	auto const num_vertices = static_cast<GLsizei>(get_indices().size());
	glDrawElementsInstanced(GL_TRIANGLES, num_vertices, GL_UNSIGNED_INT, 0, static_cast<GLsizei>(amount));

	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
}


GLuint
Mesh::get_VAO() const {
	return m_VAO;
}


Vertices
Mesh::get_vertices() const {
	return m_vertices;
}


Indices
Mesh::get_indices() const {
	return m_indices;
}


Textures
Mesh::get_textures() const {
	return m_textures;
}


void
Mesh::setup_mesh() {
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_EBO);

	glBindVertexArray(get_VAO());
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(m_vertices.size() * sizeof(Vertex)), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(m_indices.size() * sizeof(GLuint)), &m_indices[0], GL_STATIC_DRAW);

#define TO_SIZE(TYPE, MEMBER) sizeof(TYPE::MEMBER) / sizeof(float)
#define TO_OFFSET(TYPE, MEMBER) reinterpret_cast<void const*>(offsetof(TYPE, MEMBER))
#define ATTRIBUTE_PTR(LOC, TYPE, MEMBER) glVertexAttribPointer(LOC, TO_SIZE(TYPE, MEMBER), GL_FLOAT, GL_FALSE, sizeof(TYPE), TO_OFFSET(TYPE, MEMBER))
#define BIND_ATTRIBUTE_PTR(LOC, TYPE, MEMBER) \
	glEnableVertexAttribArray(LOC);           \
	ATTRIBUTE_PTR(LOC, TYPE, MEMBER)

	BIND_ATTRIBUTE_PTR(0, Vertex, position);
	BIND_ATTRIBUTE_PTR(1, Vertex, normal);
	BIND_ATTRIBUTE_PTR(2, Vertex, tex_coords);
	BIND_ATTRIBUTE_PTR(3, Vertex, tangent);
	BIND_ATTRIBUTE_PTR(4, Vertex, bitangent);

#undef TO_SIZE
#undef TO_OFFSET
#undef ATTRIBUTE_PTR
#undef BIND_ATTRIBUTE_PTR

	glBindVertexArray(0);
}
