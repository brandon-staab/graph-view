#include "Client/Renderer/ModelManager.hpp"


void
ModelManager::setup(std::string const& dir) {
	load_model(ModelKind::edge_model, dir + "edge.obj");
	load_model(ModelKind::node_model, dir + "node.obj");
}


Model const*
ModelManager::get_model(ModelKind const kind) const {
	return &m_models[static_cast<std::size_t>(kind)];
}


Model*
ModelManager::get_model(ModelKind const kind) {
	return &m_models[static_cast<std::size_t>(kind)];
}


void
ModelManager::load_model(ModelKind const kind, std::string const& path) {
	m_models[static_cast<std::size_t>(kind)] = Model(path);
}
