#include "Client/Renderer/Model.hpp"

#include <assimp/postprocess.h>
#include <SFML/Graphics.hpp>
#include <assimp/Importer.hpp>

#include <iostream>
#include <sstream>


namespace {
	GLuint
	texture_from_file(std::string const& directory, std::string const& filename) {
		auto const path = directory + '/' + filename;

		auto texture_id = GLuint{};
		glGenTextures(1, &texture_id);

		auto image = sf::Image();
		if (!image.loadFromFile(path)) {
			std::stringstream ss;
			ss << "Image failed to load \"" << path << "\".";
			throw std::runtime_error(ss.str());
		}

		unsigned char const* data = image.getPixelsPtr();
		auto const width = static_cast<GLsizei>(image.getSize().x);
		auto const height = static_cast<GLsizei>(image.getSize().y);
		auto const format = GL_RGBA;

		if (data != nullptr) {
			glBindTexture(GL_TEXTURE_2D, texture_id);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		} else {
			std::stringstream ss;
			ss << "Texture failed to load at \"" << path << "\".";
			throw std::runtime_error(ss.str());
		}

		std::clog << "Loader: \"" << path << "\"\n";

		return texture_id;
	}
}  // namespace


Model::Model(std::string const& path) : m_matrices_needs_updated(true), m_mat_buffer(), m_path(path), m_textures_loaded(), m_meshes() {
	load_model(path);
	setup_matrices();
}


void
Model::draw(Shader const& shader) const {
	if (needs_updated()) {
		send_matrices();
	}

	for (auto const& mesh : get_meshes()) {
		mesh.draw(shader, get_matrices().size());
	}
}


Meshes const&
Model::get_meshes() const {
	return m_meshes;
}


Meshes&
Model::get_meshes() {
	return m_meshes;
}


bool
Model::needs_updated() const {
	return m_matrices_needs_updated;
}


void
Model::schedule_update(bool const flag) const {
	m_matrices_needs_updated = flag;
}


Matrices&
Model::get_matrices() {
	return m_matrices;
}


Matrices const&
Model::get_matrices() const {
	return m_matrices;
}


void
Model::add_matrix(glm::mat4 const& matrix) const {
	m_matrices.push_back(matrix);
	schedule_update();
}


void
Model::clear_matrices() const {
	m_matrices.clear();
}


std::string const&
Model::get_path() const {
	return m_path;
}


std::string
Model::get_directory() const {
	return get_path().substr(0, get_path().find_last_of('/'));
}


std::string
Model::get_filename() const {
	return get_path().substr(get_path().find_last_of('/'));
}


void
Model::send_matrices() const {
	auto const payload_size = static_cast<GLsizeiptr>(get_matrices().size() * sizeof(glm::mat4));

	schedule_update(false);
	glBindBuffer(GL_ARRAY_BUFFER, m_mat_buffer);
	glBufferData(GL_ARRAY_BUFFER, payload_size, get_matrices().data(), GL_DYNAMIC_DRAW);
}


void
Model::setup_matrices() {
	auto constexpr loc = 5U;
	auto constexpr get_offset = [](auto const i) { return reinterpret_cast<void const*>(i * sizeof(glm::vec4)); };
	auto constexpr components = sizeof(glm::vec4) / sizeof(float);

	glGenBuffers(1, &m_mat_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_mat_buffer);

	for (auto const& mesh : get_meshes()) {
		glBindVertexArray(mesh.get_VAO());

		for (auto i = 0U; i < components; i++) {
			glEnableVertexAttribArray(loc + i);
			glVertexAttribPointer(loc + i, components, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), get_offset(i));
			glVertexAttribDivisor(loc + i, 1);
		}

		glBindVertexArray(0);
	}
}


// loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
void
Model::load_model(std::string const& path) {
	// read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	// check for errors
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::stringstream ss;
		ss << "ASSIMP: " << importer.GetErrorString() << '\n';
		throw std::runtime_error(ss.str());
	}

	process_node(scene->mRootNode, scene);
	std::clog << "Loader: \"" << path << "\"\n";
}


// processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void
Model::process_node(aiNode const* node, aiScene const* scene) {
	// process each mesh located at the current node
	for (auto i = 0U; i < node->mNumMeshes; i++) {
		// the node object only contains indices to index the actual objects in the scene.
		// the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		get_meshes().push_back(process_mesh(mesh, scene));
	}

	// after we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for (auto i = 0U; i < node->mNumChildren; i++) {
		process_node(node->mChildren[i], scene);
	}
}


Mesh
Model::process_mesh(aiMesh const* mesh, aiScene const* scene) {
	auto const to_vec2 = [](auto const vec2) { return glm::vec2(vec2.x, vec2.y); };
	auto const to_vec3 = [](auto const vec3) { return glm::vec3(vec3.x, vec3.y, vec3.z); };

	Vertices vertices;
	Indices indices;
	Textures textures;


	for (auto i = 0u; i < mesh->mNumVertices; i++) {
		Vertex vertex;

		vertex.position = to_vec3(mesh->mVertices[i]);
		vertex.normal = to_vec3(mesh->mNormals[i]);
		vertex.tangent = to_vec3(mesh->mTangents[i]);
		vertex.bitangent = to_vec3(mesh->mBitangents[i]);

		if (mesh->mTextureCoords[0]) {
			/* a vertex can contain up to 8 different texture coordinaes. We
			thus make the assumption that we won't use models where a vertex
			can have multiple texture coordinates so we always take the first
			set (0).
			*/
			vertex.tex_coords = to_vec2(mesh->mTextureCoords[0][i]);
		} else {
			vertex.tex_coords = glm::vec2(0.0F, 0.0F);
		}

		vertices.push_back(vertex);
	}

	for (auto i = 0U; i < mesh->mNumFaces; i++) {
		auto const face = mesh->mFaces[i];
		for (auto j = 0U; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	// process materials
	aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

	/* we assume a convention for sampler names in the shaders. Each diffuse
	texture should be named as 'texture_diffuseN' where N is a sequential number
	ranging from 1 to MAX_SAMPLER_NUMBER. Same applies to other texture as the
	following list summarizes:
	 -> diffuse: texture_diffuseN
	 -> specular: texture_specularN
	 -> normal: texture_normalN
	*/

	// diffuse maps
	Textures diffuse_maps = load_material_textures(material, aiTextureType_DIFFUSE, "texture_diffuse");
	Textures specular_maps = load_material_textures(material, aiTextureType_SPECULAR, "texture_specular");
	Textures normal_maps = load_material_textures(material, aiTextureType_HEIGHT, "texture_normal");
	Textures height_maps = load_material_textures(material, aiTextureType_AMBIENT, "texture_height");

	textures.reserve(diffuse_maps.size() + specular_maps.size() + normal_maps.size() + height_maps.size());
	textures.insert(textures.end(), normal_maps.begin(), normal_maps.end());
	textures.insert(textures.end(), diffuse_maps.begin(), diffuse_maps.end());
	textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());
	textures.insert(textures.end(), height_maps.begin(), height_maps.end());

	return Mesh(vertices, indices, textures);
}


Textures&
Model::get_loaded_textures() {
	return m_textures_loaded;
}


// checks all material textures of a given type and loads the textures if
// they're not loaded yet. The required info is returned as a Texture struct.
Textures
Model::load_material_textures(aiMaterial const* mat, aiTextureType const type, std::string const& type_name) {
	Textures textures;

	for (auto i = 0U; i < mat->GetTextureCount(type); i++) {
		aiString filename;
		mat->GetTexture(type, i, &filename);

		bool skip = false;
		for (auto const& texture : get_loaded_textures()) {
			if (std::strcmp(texture.filename.data(), filename.C_Str()) == 0) {
				textures.push_back(texture);
				skip = true;
				break;
			}
		}

		if (!skip) {
			Texture tex;
			tex.id = texture_from_file(get_directory(), filename.C_Str());
			tex.type = type_name;
			tex.filename = filename.C_Str();

			textures.push_back(tex);
			get_loaded_textures().push_back(tex);
		}
	}

	return textures;
}
