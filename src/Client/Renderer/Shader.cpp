#include "Client/Renderer/Shader.hpp"

#include "load_file.hpp"

#include <cassert>
#include <iostream>


namespace {
	void
	log_success(GLenum const type) {
		switch (type) {
			case GL_VERTEX_SHADER:
				std::clog << "OpenGL: Vertex shader compiled\n";
				break;

			case GL_FRAGMENT_SHADER:
				std::clog << "OpenGL: Fragment shader compiled\n";
				break;

			case GL_GEOMETRY_SHADER:
				std::clog << "OpenGL: Geometry shader compiled\n";
				break;

			case GL_PROGRAM:
				std::clog << "OpenGL: Program linked\n";
				break;

			default:
				assert(false);
		}
	}


	void
	check_compile_errors(GLuint const id, GLenum const type) {
		GLint success;
		char info_log[1024];

		if (type == GL_PROGRAM) {
			glGetProgramiv(id, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(id, 1024, NULL, info_log);
				std::cerr << "ERROR: GLSL_PROGRAM_LINKING_ERROR\n";
				std::cerr << info_log;
				std::cerr << "\n----------------------------------------------------------------\n";
			}
		} else {
			assert(type == GL_VERTEX_SHADER || type == GL_GEOMETRY_SHADER || type == GL_FRAGMENT_SHADER);

			glGetShaderiv(id, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(id, 1024, NULL, info_log);
				if (type == GL_VERTEX_SHADER) {
					std::cerr << "ERROR: GLSL_VERTEX_SHADER_COMPILATION_ERROR\n";
				} else if (type == GL_GEOMETRY_SHADER) {
					std::cerr << "ERROR: GLSL_GEOMETRY_SHADER_COMPILATION_ERROR\n";
				} else if (type == GL_FRAGMENT_SHADER) {
					std::cerr << "ERROR: GLSL_FRAGMENT_SHADER_COMPILATION_ERROR\n";
				}
				std::cerr << info_log;
				std::cerr << "\n----------------------------------------------------------------\n";
			}
		}

		log_success(type);
	}


	GLuint
	compile_shader(std::string const& path, GLenum const type) {
		auto const src = load_file(path).str();
		auto const* const data = src.data();
		auto shader = glCreateShader(type);
		glShaderSource(shader, 1, &data, NULL);
		glCompileShader(shader);

		check_compile_errors(shader, type);

		return shader;
	}


	GLuint
	compile_and_attach_shader(std::string const& path, GLenum const type, GLuint const id) {
		if (path.empty()) {
			return 0u;
		}

		auto const shader = compile_shader(path, type);
		glAttachShader(id, shader);

		return shader;
	}


	void
	free_shader(GLuint shader) {
		if (shader == 0u) {
			return;
		}

		glDeleteShader(shader);
	}
}  // namespace


Shader::Shader(std::string const& vertex_path, std::string const& fragment_path, std::string const& geometry_path) : m_id(glCreateProgram()) {
	auto vertex = compile_and_attach_shader(vertex_path, GL_VERTEX_SHADER, get_id());
	auto geometry = compile_and_attach_shader(geometry_path, GL_GEOMETRY_SHADER, get_id());
	auto fragment = compile_and_attach_shader(fragment_path, GL_FRAGMENT_SHADER, get_id());

	glLinkProgram(m_id);
	check_compile_errors(m_id, GL_PROGRAM);

	free_shader(vertex);
	free_shader(geometry);
	free_shader(fragment);
}


void
Shader::use() const {
	glUseProgram(m_id);
}


GLuint
Shader::get_id() const {
	return m_id;
}


GLuint
Shader::get_uniform_id(std::string const& name) const {
	return static_cast<GLuint>(glGetUniformLocation(get_id(), name.c_str()));
}


void
Shader::set_bool(std::string const& name, bool const value) const {
	set_int(name, value);
}


void
Shader::set_int(std::string const& name, int const value) const {
	glUniform1i(glGetUniformLocation(get_id(), name.c_str()), value);
}


void
Shader::set_float(std::string const& name, float const value) const {
	glUniform1f(glGetUniformLocation(get_id(), name.c_str()), value);
}


void
Shader::set_vec2(std::string const& name, glm::vec2 const& value) const {
	glUniform2fv(glGetUniformLocation(get_id(), name.c_str()), 1, &value[0]);
}


void
Shader::set_vec3(std::string const& name, glm::vec3 const& value) const {
	glUniform3fv(glGetUniformLocation(get_id(), name.c_str()), 1, &value[0]);
}


void
Shader::set_vec4(std::string const& name, glm::vec4 const& value) const {
	glUniform4fv(glGetUniformLocation(get_id(), name.c_str()), 1, &value[0]);
}


void
Shader::set_mat2(std::string const& name, glm::mat2 const& mat) const {
	glUniformMatrix2fv(glGetUniformLocation(get_id(), name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


void
Shader::set_mat3(std::string const& name, glm::mat3 const& mat) const {
	glUniformMatrix3fv(glGetUniformLocation(get_id(), name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


void
Shader::set_mat4(std::string const& name, glm::mat4 const& mat) const {
	glUniformMatrix4fv(glGetUniformLocation(get_id(), name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
