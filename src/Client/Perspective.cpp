#include "Client/Perspective.hpp"

#include "Client/Client.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <algorithm>


namespace {
	auto constexpr START_FOV = 90.0F;
	auto constexpr MIN_FOV = 30.0F;
	auto constexpr MAX_FOV = 150.0F;
}  // namespace


Perspective::Perspective(sf::Window& window, Renderer& renderer) : m_fov(START_FOV), m_window(window), m_renderer(renderer) {}


void
Perspective::update() const {
	auto const size = sf::Vector2f(m_window.getSize());
	auto const aspect_ratio = size.x / size.y;

	glm::mat4 proj = glm::perspective(glm::radians(get_fov()), aspect_ratio, 0.001F, 10000.0F);  // NOLINT
	m_renderer.get_shader().set_mat4("projection", proj);
}


void
Perspective::reset() {
	set_fov(START_FOV);
}


float
Perspective::get_fov() const {
	return m_fov;
}


void
Perspective::set_fov(float const fov) {
	m_fov = std::clamp(fov, MIN_FOV, MAX_FOV);
	update();
}


void
Perspective::add_fov(float const delta) {
	set_fov(get_fov() + delta);
}
