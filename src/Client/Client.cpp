#include "Client/Client.hpp"

#include "Physics/PhysicsManager.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>


namespace {
	void
	check_args(std::span<char const* const> args) noexcept(false) {
		if (args.size() != 2) {
			for (auto i = 0U; i < args.size(); i++) {
				std::clog << "args[" << i << "] = " << args[i] << '\n';
			}

			std::stringstream ss;
			ss << "Usage: " << args[0] << " <filename>\n";
			throw std::runtime_error(ss.str());
		}
	}
}  // namespace


Client::Client(std::span<char const* const> args) noexcept(false) : m_player(&m_physics_manager), m_renderer(*this) {
	check_args(args);

	// Setup console
	std::cout << std::setprecision(4);
	std::clog << std::setprecision(4);
	std::cerr << std::setprecision(4);

	// Load graph
	auto const path = std::string(args[1]);
	m_graph = std::make_unique<Graph>(&m_physics_manager, path);

	// Context
	sf::ContextSettings settings;
	settings.depthBits = 24;         // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	settings.stencilBits = 8;        // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	settings.antialiasingLevel = 8;  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	settings.majorVersion = 4;       // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	settings.minorVersion = 6;       // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	settings.attributeFlags = sf::ContextSettings::Core;

	// Window
	m_window.create(sf::VideoMode(1600, 900), "Graph View", sf::Style::Default, settings);  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	m_window.setVerticalSyncEnabled(true);
	m_window.setFramerateLimit(60);  // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	m_window.setKeyRepeatEnabled(false);
	m_window.setMouseCursorVisible(false);
	m_window.setMouseCursorGrabbed(true);

	auto center = sf::Vector2i(get_window().getSize()) / 2;
	sf::Mouse::setPosition(center, get_window());

	// GLEW
	glewExperimental = GL_TRUE;
	if (auto error_code = glewInit(); error_code != GLEW_OK) {
		std::stringstream ss;
		ss << "GLEW: " << glewGetErrorString(error_code);
		throw std::runtime_error(ss.str());
	}

	std::clog << "GLEW: Version " << glewGetString(GLEW_VERSION);
	std::clog << "\nOpenGL: Version " << glGetString(GL_VERSION) << '\n';
	get_renderer().setup();
}


void
Client::run() {
	while (get_window().isOpen()) {
		handle_events();
		tick_physics();
		draw();
	}
}


void
Client::handle_events() {
	auto event = sf::Event{};

	while (get_window().pollEvent(event)) {
		if (auto const* cmd = m_event_handler.handle(event)) {
			cmd->execute(this);
		}
	}
}


void
Client::tick_physics() {
	get_physics_manager().tick();
	get_player().tick();
	get_graph().tick(&get_renderer());
}


void
Client::draw() {
	update_camera();
	get_renderer().draw();
	get_window().display();
}


Graph const&
Client::get_graph() const noexcept {
	return *m_graph;
}


Graph&
Client::get_graph() noexcept {
	return *m_graph;
}


sf::Window&
Client::get_window() {
	return m_window;
}


void
Client::close() {
	get_window().close();
}


Renderer&
Client::get_renderer() {
	return m_renderer;
}


EventHandler&
Client::get_event_handler() {
	return m_event_handler;
}


void
Client::exec_cmd(Command::Kind const cmd_kind) {
	if (auto const* cmd = get_event_handler().get_cmd(cmd_kind)) {
		cmd->execute(this);
	}
}


PhysicsManager&
Client::get_physics_manager() {
	return m_physics_manager;
}


Player&
Client::get_player() {
	return m_player;
}


PlayerCamera&
Client::get_camera() {
	return m_player.get_camera();
}


void
Client::update_camera() {
	get_camera().update(&get_renderer());
}


void
Client::update_perspective() {
	m_perspective.update();
}


Perspective&
Client::get_perspective() {
	return m_perspective;
}
