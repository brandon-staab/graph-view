#include "Client/Client.hpp"

#include <iostream>


[[nodiscard]] int
main([[maybe_unused]] int const argc, [[maybe_unused]] char const* const* const argv) noexcept {
	try {
		auto args = std::span{argv, static_cast<std::size_t>(argc)};
		auto client = Client{args};
		client.run();
	} catch (std::exception const& e) {
		std::cerr << "CRITICAL ERROR: " << e.what() << '\n';
		return EXIT_FAILURE;
	}
}
