#include "Physics/PhysicsProperties.hpp"

#include <glm/gtx/rotate_vector.hpp>


namespace {
	auto constexpr UP = glm::vec3(0.0F, 1.0F, 0.0F);
}


PhysicsProperties::PhysicsProperties(Kind const kind) : m_kind(kind), m_pos(), m_facing(glm::vec3(0.0F, 0.0F, -1.0F)) {}


PhysicsProperties::Kind
PhysicsProperties::get_kind() const {
	return m_kind;
}


void
PhysicsProperties::tick_physics() {
	// no-op
}


glm::vec3
PhysicsProperties::get_pos() const {
	return m_pos;
}


void
PhysicsProperties::set_pos(glm::vec3 const pos) {
	m_pos = pos;
}


void
PhysicsProperties::add_pos(glm::vec3 const pos) {
	m_pos += pos;
}


glm::vec3
PhysicsProperties::get_facing() const {
	return m_facing;
}


void
PhysicsProperties::set_facing(glm::vec3 const facing) {
	m_facing = facing;
}


void
PhysicsProperties::spin_facing(glm::vec3 const spin) {
	m_facing = glm::rotate(get_facing(), spin.x, get_up());
	m_facing = glm::rotate(get_facing(), spin.y, get_right());
}


glm::vec3
PhysicsProperties::get_forward() const {
	return get_facing();
}


glm::vec3
PhysicsProperties::get_up() const {
	return UP;
}


glm::vec3
PhysicsProperties::get_right() const {
	return glm::cross(get_facing(), get_up());
}
