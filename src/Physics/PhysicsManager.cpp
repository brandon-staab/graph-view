#include "Physics/PhysicsManager.hpp"

#include "Physics/AdvancedPhysicsProperties.hpp"


void
PhysicsManager::tick() {
	for (auto& obj : get_objects()) {
		obj->tick_physics();
	}
}


PhysicsProperties*
PhysicsManager::create(Entity::Kind const kind) {
	switch (kind) {
		case Entity::Kind::graph_entity:
		case Entity::Kind::edge_entity:
			return get_objects().emplace_back(std::make_unique<PhysicsProperties>()).get();

		case Entity::Kind::node_entity:
			return get_objects().emplace_back(std::make_unique<BasicPhysicsProperties>()).get();

		case Entity::Kind::player_entity:
			return get_objects().emplace_back(std::make_unique<AdvancedPhysicsProperties>()).get();

		default:
			assert(false);
	}

	std::abort();
}


std::vector<std::unique_ptr<PhysicsProperties>>&
PhysicsManager::get_objects() {
	return m_objects;
}

