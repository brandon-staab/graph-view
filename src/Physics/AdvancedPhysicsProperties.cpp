#include "Physics/AdvancedPhysicsProperties.hpp"

#include <cassert>


namespace {
	auto constexpr ACCELERATION_SCALE = 0.40F;
	auto constexpr REDUCE_ACCELERATION_SCALE = 0.0F;  // Not used
	auto constexpr REDUCE_TORQUE_SCALE = 0.5F;
}  // namespace


AdvancedPhysicsProperties::AdvancedPhysicsProperties() : BasicPhysicsProperties(Kind::adv_props), m_acceleration(), m_torque() {}


void
AdvancedPhysicsProperties::tick_physics() {
	add_velocity(get_abs_acceleration());
	add_spin(get_torque());

	BasicPhysicsProperties::tick_physics();
}


glm::vec3
AdvancedPhysicsProperties::get_acceleration() const {
	return m_acceleration;
}


glm::vec3
AdvancedPhysicsProperties::get_abs_acceleration() const {
	if (get_acceleration() == glm::vec3()) {
		return glm::vec3();
	}

	auto const f = get_facing();
	auto const a = glm::normalize(get_acceleration()) * ACCELERATION_SCALE;

	return a.x * get_right()                   //
		   + a.y * get_up()                    //
		   - a.z * glm::vec3(f.x, 0.0F, f.z);  // FIXME: decouple from player
}


void
AdvancedPhysicsProperties::set_acceleration(glm::vec3 const acceleration) {
	m_acceleration = acceleration;
}


void
AdvancedPhysicsProperties::add_acceleration(glm::vec3 const acceleration) {
	m_acceleration += acceleration;
}


void
AdvancedPhysicsProperties::scale_acceleration(float const factor) {
	m_acceleration *= factor;
}


void
AdvancedPhysicsProperties::reduce_acceleration() {
	assert(false);
	scale_acceleration(REDUCE_ACCELERATION_SCALE);
}


glm::vec3
AdvancedPhysicsProperties::get_torque() const {
	return m_torque;
}


void
AdvancedPhysicsProperties::set_torque(glm::vec3 const torque) {
	m_torque = torque;
}


void
AdvancedPhysicsProperties::add_torque(glm::vec3 const torque) {
	m_torque += torque;
}


void
AdvancedPhysicsProperties::scale_torque(float const factor) {
	m_torque *= factor;
}


void
AdvancedPhysicsProperties::reduce_torque() {
	scale_torque(REDUCE_TORQUE_SCALE);
}
