#include "Physics/BasicPhysicsProperties.hpp"


namespace {
	auto constexpr REDUCE_VELOCITY_SCALE = 0.9F;
	auto constexpr REDUCE_SPIN_SCALE = 0.6F;
}  // namespace


BasicPhysicsProperties::BasicPhysicsProperties(Kind const kind) : PhysicsProperties(kind), m_velocity(), m_spin() {}


void
BasicPhysicsProperties::tick_physics() {
	add_pos(get_velocity());
	spin_facing(get_spin());

	reduce_velocity();
	reduce_spin();

	PhysicsProperties::tick_physics();
}


glm::vec3
BasicPhysicsProperties::get_velocity() const {
	return m_velocity;
}


void
BasicPhysicsProperties::set_velocity(glm::vec3 const velocity) {
	m_velocity = velocity;
}


void
BasicPhysicsProperties::add_velocity(glm::vec3 const velocity) {
	m_velocity += velocity;
}


void
BasicPhysicsProperties::scale_velocity(float const factor) {
	m_velocity *= factor;
}


void
BasicPhysicsProperties::reduce_velocity() {
	scale_velocity(REDUCE_VELOCITY_SCALE);
}


glm::vec3
BasicPhysicsProperties::get_spin() const {
	return m_spin;
}


void
BasicPhysicsProperties::set_spin(glm::vec3 const spin) {
	m_spin = spin;
}


void
BasicPhysicsProperties::add_spin(glm::vec3 const spin) {
	m_spin += spin;
}


void
BasicPhysicsProperties::scale_spin(float const factor) {
	m_spin *= factor;
}


void
BasicPhysicsProperties::reduce_spin() {
	scale_spin(REDUCE_SPIN_SCALE);
}
