#include "Command/Reset_cmd.hpp"

#include "Client/Client.hpp"


Reset_cmd::Reset_cmd() : Command(Kind::reset_cmd) {}


void
Reset_cmd::execute(Client* const client) const {
	auto constexpr zero = glm::vec3();
	auto* properties = dynamic_cast<AdvancedPhysicsProperties*>(client->get_player().get_physics());
	assert(properties != nullptr);

	client->get_perspective().reset();
	properties->set_facing(glm::vec3(0.0F, 0.0F, -1.0F));
	properties->set_velocity(zero);
	properties->set_spin(zero);
	properties->set_acceleration(zero);
	properties->set_torque(zero);
}
