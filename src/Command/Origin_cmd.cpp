#include "Command/Origin_cmd.hpp"

#include "Client/Client.hpp"


Origin_cmd::Origin_cmd() : Command(Kind::origin_cmd) {}


void
Origin_cmd::execute(Client* const client) const {
	auto* properties = dynamic_cast<AdvancedPhysicsProperties*>(client->get_player().get_physics());
	assert(properties != nullptr);

	client->exec_cmd(Kind::reset_cmd);
	properties->set_pos(glm::vec3(0.0F, 0.0F, 0.0F));
}
