#include "Command/Close_cmd.hpp"

#include "Client/Client.hpp"


Close_cmd::Close_cmd() : Command(Kind::close_cmd) {}


void
Close_cmd::execute(Client* const client) const {
	client->close();
}
