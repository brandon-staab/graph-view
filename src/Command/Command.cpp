#include "Command/Command.hpp"


Command::Command(Kind const kind) : m_kind(kind) {}


Command::Kind
Command::get_kind() const {
	return m_kind;
}

