#include "Command/Acceleration_cmd.hpp"

#include "Client/Client.hpp"

#include <cassert>


namespace {
	auto constexpr ACCELERATION_SCALE = 0.01f;


	glm::vec3
	cmd_to_vec(Command::Kind const cmd_kind) {
		switch (cmd_kind) {
			case Command::Kind::left_acceleration_cmd:
				return glm::vec3(-1, 0, 0);

			case Command::Kind::right_acceleration_cmd:
				return glm::vec3(1, 0, 0);

			case Command::Kind::front_acceleration_cmd:
				return glm::vec3(0, 0, -1);

			case Command::Kind::back_acceleration_cmd:
				return glm::vec3(0, 0, 1);

			case Command::Kind::up_acceleration_cmd:
				return glm::vec3(0, 1, 0);

			case Command::Kind::down_acceleration_cmd:
				return glm::vec3(0, -1, 0);

			default:
				assert(false);
		}
	}
}  // namespace


Acceleration_cmd::Acceleration_cmd(Kind const kind) : Command(kind) {
	assert(Kind::left_acceleration_cmd <= get_kind() && get_kind() <= Kind::down_acceleration_cmd);
}


void
Acceleration_cmd::execute(Client* const client) const {
	auto* property = dynamic_cast<AdvancedPhysicsProperties*>(client->get_player().get_physics());

	assert(property != nullptr);
	assert(property->get_kind() == PhysicsProperties::Kind::adv_props);

	return property->add_acceleration(ACCELERATION_SCALE * cmd_to_vec(get_kind()));
}
