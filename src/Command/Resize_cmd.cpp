#include "Command/Resize_cmd.hpp"

#include "Client/Client.hpp"


Resize_cmd::Resize_cmd() : Command(Kind::resize_cmd) {}


void
Resize_cmd::execute(Client* const client) const {
	auto const size = sf::Vector2i(client->get_window().getSize());

	glViewport(0, 0, size.x, size.y);
	client->update_perspective();
}
