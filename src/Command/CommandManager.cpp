#include "Command/CommandManager.hpp"

#include "Command/Acceleration_cmd.hpp"
#include "Command/Close_cmd.hpp"
#include "Command/Fov_cmd.hpp"
#include "Command/Origin_cmd.hpp"
#include "Command/Pan_cmd.hpp"
#include "Command/Reset_cmd.hpp"
#include "Command/Resize_cmd.hpp"

#include <cassert>


CommandManager::CommandManager() : m_table() {
	using K = Command::Kind;
	std::generate(std::begin(m_table), std::end(m_table), []() { return std::unique_ptr<Command>(nullptr); });

	m_table[std::size_t(K::close_cmd)] = std::make_unique<Close_cmd>();
	m_table[std::size_t(K::resize_cmd)] = std::make_unique<Resize_cmd>();

	m_table[std::size_t(K::reset_cmd)] = std::make_unique<Reset_cmd>();
	m_table[std::size_t(K::origin_cmd)] = std::make_unique<Origin_cmd>();
	m_table[std::size_t(K::pan_cmd)] = std::make_unique<Pan_cmd>();
	m_table[std::size_t(K::pos_fov_cmd)] = std::make_unique<Fov_cmd>(K::pos_fov_cmd);
	m_table[std::size_t(K::neg_fov_cmd)] = std::make_unique<Fov_cmd>(K::neg_fov_cmd);

	m_table[std::size_t(K::left_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::left_acceleration_cmd);
	m_table[std::size_t(K::right_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::right_acceleration_cmd);
	m_table[std::size_t(K::front_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::front_acceleration_cmd);
	m_table[std::size_t(K::back_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::back_acceleration_cmd);
	m_table[std::size_t(K::up_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::up_acceleration_cmd);
	m_table[std::size_t(K::down_acceleration_cmd)] = std::make_unique<Acceleration_cmd>(K::down_acceleration_cmd);
}


Command const*
CommandManager::get(Command::Kind const kind) const {
	auto const idx = static_cast<std::size_t>(kind);
	assert(idx < m_table.size());

	return m_table[idx].get();
}
