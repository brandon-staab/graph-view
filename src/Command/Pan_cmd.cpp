#include "Command/Pan_cmd.hpp"

#include "Client/Client.hpp"

#include <glm/glm.hpp>


namespace {
	auto constexpr PAN_SCALE = -0.0005F;
}


Pan_cmd::Pan_cmd() : Command(Kind::pan_cmd) {}


void
Pan_cmd::execute(Client* const client) const {
	auto& w = client->get_window();
	auto center = sf::Vector2i(w.getSize()) / 2;
	auto delta = sf::Mouse::getPosition(w) - center;

	if (delta == sf::Vector2i()) {
		return;
	}

	auto* properties = dynamic_cast<AdvancedPhysicsProperties*>(client->get_player().get_physics());
	assert(properties != nullptr);
	properties->add_torque(PAN_SCALE * glm::vec3(delta.x, delta.y, 0.0F));

	sf::Mouse::setPosition(center, w);
}
