#include "Command/Fov_cmd.hpp"

#include "Client/Client.hpp"

#include <glm/glm.hpp>

#include <cassert>


namespace {
	auto constexpr FOV_SCALE = 5.0F;
}


Fov_cmd::Fov_cmd(Kind const kind) : Command(kind) {
	assert(get_kind() == Kind::pos_fov_cmd || get_kind() == Kind::neg_fov_cmd);
}


void
Fov_cmd::execute(Client* const client) const {
	auto& p = client->get_perspective();

	switch (get_kind()) {
		case Kind::pos_fov_cmd:
			return p.add_fov(FOV_SCALE);

		case Kind::neg_fov_cmd:
			return p.add_fov(-FOV_SCALE);

		default:
			assert(false);
	}
}
