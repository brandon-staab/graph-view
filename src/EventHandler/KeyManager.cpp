#include "EventHandler/KeyManager.hpp"

#include <cassert>


namespace {
	auto constexpr RELEASED = 1U;
}


KeyManager::KeyManager() : m_table() {
	m_table.fill(nullptr);
}


Command const*
KeyManager::get_pressed_bind(sf::Keyboard::Key const key) const {
	if (key == sf::Keyboard::Key::Unknown) {
		return nullptr;
	}

	auto const idx = 2 * static_cast<std::size_t>(key);
	assert(idx >= 0);
	assert(idx < m_table.size());

	return m_table[idx];
}


Command const*
KeyManager::get_released_bind(sf::Keyboard::Key const key) const {
	if (key == sf::Keyboard::Key::Unknown) {
		return nullptr;
	}

	auto const idx = 2 * static_cast<std::size_t>(key) + RELEASED;
	assert(idx >= 0);
	assert(idx < m_table.size());

	return m_table[idx];
}


void
KeyManager::set_key_pressed_bind(sf::Keyboard::Key const key, Command const* const cmd) {
	if (key == sf::Keyboard::Key::Unknown) {
		return;
	}

	auto const idx = 2 * static_cast<std::size_t>(key);
	assert(idx >= 0);
	assert(idx < m_table.size());

	m_table[idx] = cmd;
}


void
KeyManager::set_key_released_bind(sf::Keyboard::Key const key, Command const* const cmd) {
	if (key == sf::Keyboard::Key::Unknown) {
		return;
	}
	auto const idx = 2 * static_cast<std::size_t>(key) + RELEASED;
	assert(idx >= 0);
	assert(idx < m_table.size());

	m_table[idx] = cmd;
}
