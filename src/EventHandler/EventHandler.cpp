#include "EventHandler/EventHandler.hpp"

#include "Client/Client.hpp"


EventHandler::EventHandler() : m_cmd_mgr(), m_key_mgr() {
	using Key = sf::Keyboard::Key;
	using Cmd = Command::Kind;

	set_key_pressed_bind(Key::Escape, Cmd::close_cmd);
	set_key_pressed_bind(Key::R, Cmd::reset_cmd);
	set_key_pressed_bind(Key::O, Cmd::origin_cmd);

	auto acceleration_bind = [&](Key const key, Cmd const cmd_down, Cmd const cmd_up) {
		set_key_pressed_bind(key, cmd_down);
		set_key_released_bind(key, cmd_up);
	};

	acceleration_bind(Key::A, Cmd::left_acceleration_cmd, Cmd::right_acceleration_cmd);
	acceleration_bind(Key::D, Cmd::right_acceleration_cmd, Cmd::left_acceleration_cmd);
	acceleration_bind(Key::W, Cmd::front_acceleration_cmd, Cmd::back_acceleration_cmd);
	acceleration_bind(Key::S, Cmd::back_acceleration_cmd, Cmd::front_acceleration_cmd);
	acceleration_bind(Key::Space, Cmd::up_acceleration_cmd, Cmd::down_acceleration_cmd);
	acceleration_bind(Key::LShift, Cmd::down_acceleration_cmd, Cmd::up_acceleration_cmd);
}


Command const*
EventHandler::get_cmd(Command::Kind const cmd_kind) const {
	return m_cmd_mgr.get(cmd_kind);
}


Command const*
EventHandler::handle(sf::Event const& event) const {
	switch (event.type) {
		case sf::Event::Closed:
			return get_cmd(Command::Kind::close_cmd);

		case sf::Event::Resized:
			return get_cmd(Command::Kind::resize_cmd);

		case sf::Event::MouseMoved:
			return get_cmd(Command::Kind::pan_cmd);

		case sf::Event::MouseWheelScrolled:
			if (event.mouseWheelScroll.delta < 0) {
				return get_cmd(Command::Kind::pos_fov_cmd);
			} else {
				return get_cmd(Command::Kind::neg_fov_cmd);
			}

		case sf::Event::KeyPressed:
			return handle_key_pressed(event.key);

		case sf::Event::KeyReleased:
			return handle_key_released(event.key);

		default:
			return nullptr;
	}
}


Command const*
EventHandler::handle_key_pressed(sf::Event::KeyEvent const& key_event) const {
	return m_key_mgr.get_pressed_bind(key_event.code);
}


Command const*
EventHandler::handle_key_released(sf::Event::KeyEvent const& key_event) const {
	return m_key_mgr.get_released_bind(key_event.code);
}


void
EventHandler::set_key_pressed_bind(sf::Keyboard::Key const key, Command::Kind const cmd_kind) {
	m_key_mgr.set_key_pressed_bind(key, m_cmd_mgr.get(cmd_kind));
}


void
EventHandler::set_key_released_bind(sf::Keyboard::Key const key, Command::Kind const cmd_kind) {
	m_key_mgr.set_key_released_bind(key, m_cmd_mgr.get(cmd_kind));
}

