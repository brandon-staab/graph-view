#include "Entity/Physics_entity.hpp"

#include "Client/Client.hpp"


// nullptr adds support for boost to create dummy objects
Physics_entity::Physics_entity(Kind const kind, PhysicsManager* const physics_manager) noexcept :
	Entity(kind), m_physics(physics_manager == nullptr ? nullptr : physics_manager->create(kind)) {}


PhysicsProperties::Kind
Physics_entity::get_physics_kind() const noexcept {
	return get_physics()->get_kind();
}


PhysicsProperties const*
Physics_entity::get_physics() const noexcept {
	assert(m_physics != nullptr);
	return m_physics;
}


PhysicsProperties*
Physics_entity::get_physics() noexcept {
	assert(m_physics != nullptr);
	return m_physics;
}
