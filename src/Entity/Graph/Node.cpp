#include "Entity/Graph/Node.hpp"

#include "Physics/BasicPhysicsProperties.hpp"

#include <glm/ext/matrix_transform.hpp>


Node::Node() noexcept : Physics_entity(Kind::node_entity, nullptr) {}


Node::Node(PhysicsManager* physics_manager, glm::vec3 const pos) noexcept : Physics_entity(Kind::node_entity, physics_manager) {
	set_pos(pos);
}


glm::mat4
Node::get_matrix() const noexcept {
	return glm::scale(glm::translate(glm::mat4(1), get_pos()), glm::vec3(2.0F));
}


void
Node::set_pos(glm::vec3 const pos) noexcept {
	get_physics()->set_pos(pos);
}


glm::vec3
Node::get_pos() const noexcept {
	return get_physics()->get_pos();
}


void
Node::set_velocity(glm::vec3 const delta) noexcept {
	auto* property_ptr = dynamic_cast<BasicPhysicsProperties*>(get_physics());
	assert(property_ptr != nullptr);

	property_ptr->set_velocity(delta);
}


void
Node::add_velocity(glm::vec3 const delta) noexcept {
	auto* property_ptr = dynamic_cast<BasicPhysicsProperties*>(get_physics());
	assert(property_ptr != nullptr);

	property_ptr->add_velocity(delta);
}


glm::vec3
Node::get_velocity() const noexcept {
	auto const* const property_ptr = dynamic_cast<BasicPhysicsProperties const* const>(get_physics());
	assert(property_ptr != nullptr);

	return property_ptr->get_velocity();
}
