#include "Entity/Graph/Graph.hpp"

#include <glm/gtc/random.hpp>

#include <cmath>
#include <iostream>


namespace {
	std::pair<float, glm::vec3>
	decompose(glm::vec3 const here, glm::vec3 const there) noexcept {
		auto const rejection = there - here;
		auto const magnitude = glm::length(rejection);
		auto const direction = glm::normalize(rejection);

		return {magnitude, direction};
	}


	glm::vec3
	coulomb_law(glm::vec3 const here, glm::vec3 const there) noexcept {
		auto const [distance, direction] = decompose(here, there);
		auto const inverse_square = 1.0F / std::sqrt(distance);

		auto const scaler = 0.1F * std::log(inverse_square + 1.0F);

		return scaler * direction;
	}


	/**
	 * @param length The length of an edge.
	 * @param ideal The ideal length of an edge.
	 * @param factor How quickly the cubic function will grow.
	 * @param bias A bias towards an edge length.  A positive number means that
	 * a more aggressive scaler will be returned if the edge is to long.
	 * @return A scalare for the magnitude a node needs to move to reach the
	 * ideal edge length.
	 */
	float
	cubic_scaler(float const length, float const ideal, float const factor, float const bias) noexcept {
		assert(factor > 0.0F);

		auto const shift = factor * ideal - std::cbrt(bias);
		auto const base = shift - factor * length;

		return std::pow(base, 3.0F) + bias;
	}


	glm::vec3
	hooke_law(glm::vec3 const here, glm::vec3 const there) noexcept {
		auto constexpr IDEAL = 3.0F;
		auto constexpr FACTOR = 0.1F;
		auto constexpr BIAS = 0.0002F;

		auto const [distance, direction] = decompose(here, there);
		auto const displacement = distance - IDEAL;

		auto const scaler = cubic_scaler(displacement, IDEAL, FACTOR, BIAS);

		return -0.01F * scaler * direction;
	}
}  // namespace


void
Graph::update_node_positions() noexcept {
	auto& graph = get_boost_graph();
	auto const num_nodes = static_cast<float>(boost::num_vertices(graph));
	auto const centering_radius = std::pow(num_nodes, 2.0F);
	auto const graph_pos = get_physics()->get_pos();
	auto graph_pos_new = glm::vec3();

	for (auto [node_iter, node_iter_end] = get_nodes(); node_iter != node_iter_end; node_iter++) {
		auto& node = graph[*node_iter];
		auto const node_pos = node.get_pos();

		// push nodes apart
		for (auto other_node_iter = node_iter + 1, other_node_iter_end = node_iter_end; other_node_iter != other_node_iter_end; other_node_iter++) {
			auto& other_node = get_boost_graph()[*other_node_iter];
			auto const other_node_pos = other_node.get_pos();

			// add equal and opposite charge forces
			auto const force = coulomb_law(node_pos, other_node_pos);
			node.add_velocity(-force);
			other_node.add_velocity(force);
		}

		// pull connected nodes together
		for (auto [node_edge_iter, node_edge_iter_end] = boost::out_edges(*node_iter, graph); node_edge_iter != node_edge_iter_end; node_edge_iter++) {
			auto const other_node_iter = boost::target(*node_edge_iter, graph);
			auto& other_node = graph[other_node_iter];
			auto const other_node_pos = other_node.get_pos();

			// springs
			node.add_velocity(hooke_law(node_pos, other_node_pos));

			// charge
			node.add_velocity(0.9F * coulomb_law(node_pos, other_node_pos));

			// push second neighbors apart
			for (auto [other_node_edge_iter, other_node_edge_iter_end] = boost::out_edges(other_node_iter, graph);  //
				 other_node_edge_iter != other_node_edge_iter_end; other_node_edge_iter++) {
				auto const other_node2_iter = boost::target(*other_node_edge_iter, graph);
				auto& other_node2 = graph[other_node2_iter];

				// skip if self
				if (&node == &other_node2) {
					continue;
				}
				auto const other_node2_pos = other_node2.get_pos();

				// charge
				node.add_velocity(-0.8F * coulomb_law(node_pos, other_node2_pos));
			}
		}

		// fix nans
		if (std::isnan(glm::length(node.get_velocity()))) {
			node.set_velocity(glm::vec3());
		}
		if (std::isnan(glm::length(node.get_pos()))) {
			node.set_velocity(glm::sphericalRand(16.0F));
		}

		// centering node
		if (auto const error = glm::length(node_pos) - centering_radius; error > 0.0F) {
			node.add_velocity(std::max(-0.01F * std::pow(error, 2.0F), -0.1F) * node_pos);
		}

		// centering graph
		graph_pos_new -= node_pos;
		auto const graph_centering = 0.001F * graph_pos;
		node.get_physics()->add_pos(graph_centering);

		m_delta += glm::length(node.get_velocity());
	}

	get_physics()->set_pos(graph_pos_new);
	m_delta /= num_nodes;
	if (std::isnan(m_delta)) {
		m_delta = 100.0F;
	}
}
