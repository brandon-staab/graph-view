#include "Entity/Graph/Edge.hpp"

#include <glm/ext/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>


Edge::Edge() noexcept : Physics_entity(Kind::edge_entity, nullptr) {}

Edge::Edge(PhysicsManager* physics_manager, glm::vec3 const start, glm::vec3 const end) noexcept : Physics_entity(Kind::edge_entity, physics_manager) {
	set_location(start, end);
}


glm::mat4
Edge::get_matrix() const noexcept {
	auto constexpr cube_initial = glm::vec3(-0.5F, 0.0F, 0.0F);
	auto constexpr cube_terminal = glm::vec3(+0.5F, 0.0F, 0.0F);
	auto constexpr cube_rejection = cube_terminal - cube_initial;
	auto constexpr cube_rejection_normal = cube_rejection;
	assert(cube_rejection_normal == glm::normalize(cube_rejection));

	auto const& physics = *get_physics();

	auto const edge_initial = physics.get_pos();
	auto const edge_terminal = physics.get_facing();
	auto const edge_rejection = edge_terminal - edge_initial;
	auto const edge_rejection_normal = glm::normalize(edge_rejection);
	auto const edge_center = (edge_initial + edge_terminal) / 2.0F;

	auto model = glm::mat4(1.0F);

	// Tanslate
	model = glm::translate(model, edge_center);

	// Rotate
	auto const normal = glm::cross(cube_rejection_normal, edge_rejection_normal);
	if (normal != glm::vec3(0)) {
		auto const angle = glm::angle(cube_rejection_normal, edge_rejection_normal);
		model = rotate(model, angle, normal);
	}

	// Scale
	auto constexpr thickness = 0.20F;
	auto const length = 0.5F * distance(edge_initial, edge_terminal);
	model = scale(model, glm::vec3(length, thickness, thickness));

	return model;
}


void
Edge::set_location(glm::vec3 const start, glm::vec3 const end) noexcept {
	get_physics()->set_pos(start);
	get_physics()->set_facing(end);
}
