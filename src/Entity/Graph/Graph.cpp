#include "Entity/Graph/Graph.hpp"

#include "Client/Client.hpp"
#include "load_file.hpp"

#include <glm/gtc/random.hpp>
#include <glm/gtx/component_wise.hpp>

#include <iostream>
#include <limits>


namespace {
	void
	add_vertex(PhysicsManager* physics_manager, Graph::BoostGraph& graph) noexcept {
		auto constexpr radius = 64.0F;
		auto const vertex = Node{physics_manager, glm::sphericalRand(radius)};
		boost::add_vertex(vertex, graph);
	}


	void
	add_edge(PhysicsManager* physics_manager, Graph::BoostGraph& graph, unsigned const x, unsigned const y) {
		auto const I = graph[x].get_pos();
		auto const T = graph[y].get_pos();
		auto const edge = Edge{physics_manager, I, T};

		auto [edge_itr, ok] = boost::add_edge(x, y, edge, graph);
		if (!ok) {
			std::stringstream ss;
			ss << "Failed to add edge (" << x << ", " << y << ") the to graph.";
			return;
			throw std::runtime_error(ss.str());
		}
	}


	void
	load_graph(PhysicsManager* physics_manager, std::string const& path, Graph::BoostGraph& graph) {
		auto is = load_file(path);

		std::stringstream ss;
		ss.exceptions(std::stringstream::failbit | std::stringstream::badbit);
		std::string line;
		line.reserve(512U);

		auto const get_line = [&]() {
			if (!is.good() || !is.good()) {
				return false;
			}
			if (std::getline(is, line)) {
				ss.str(line);
				ss.clear();

				return true;
			}

			return false;
		};


		// reserve
		auto num_nodes = 0U;
		get_line();
		ss >> num_nodes;

		for (auto i = 0U; i < num_nodes; i++) {
			add_vertex(physics_manager, graph);
		}

		// read
		auto initial = -1U;
		auto terminal = 0U;
		while (get_line()) {
			initial++;
			if (line.empty()) {
				continue;
			}
			while (ss.good()) {
				ss >> terminal;
				if (initial > num_nodes || terminal > num_nodes) {
					ss.str();
					ss.clear();
					ss << "Failed to add edge (" << initial << ", " << terminal << ") the to graph of size " << num_nodes << '.';
					throw std::runtime_error(ss.str());
				}
				add_edge(physics_manager, graph, initial, terminal);
			}
		}
	}
}  // namespace


Graph::Graph(PhysicsManager* physics_manager, std::string const& path) : Physics_entity(Kind::graph_entity, physics_manager), m_iterations(1U), m_delta(1.0F), m_boost_graph() {
	load_graph(physics_manager, path, m_boost_graph);
}


void
Graph::tick(Renderer* const renderer) noexcept {
	if (m_iterations == 0U) {
		return;
	}

	update_node_positions();
	update_edge_positions();
	renderer->schedule_update();

	if (m_iterations % 50U == 0U) {
		std::clog << "\r                                                 \rIteration: " << m_iterations << ", Delta: " << m_delta;
	}

	m_iterations++;

	if (m_delta < 1.0e-4F) {
		std::clog << "\nDone!\n";
		m_iterations = 0U;
	}
}


Graph::BoostGraph const&
Graph::get_boost_graph() const noexcept {
	return m_boost_graph;
}


Graph::BoostGraph&
Graph::get_boost_graph() noexcept {
	return m_boost_graph;
}


Graph::EdgeIters
Graph::get_edges() const noexcept {
	return boost::edges(get_boost_graph());
}


Graph::EdgeIters
Graph::get_edges() noexcept {
	return boost::edges(get_boost_graph());
}


Graph::VertexIters
Graph::get_nodes() const noexcept {
	return boost::vertices(get_boost_graph());
}


Graph::VertexIters
Graph::get_nodes() noexcept {
	return boost::vertices(get_boost_graph());
}


void
Graph::update_edge_positions() noexcept {
	auto& graph = get_boost_graph();

	for (auto [iter, end] = get_edges(); iter != end; iter++) {
		auto const edge_key = *iter;

		auto const initial_node = boost::source(edge_key, graph);
		auto const terminal_node = boost::target(edge_key, graph);

		auto const initial_pos = graph[initial_node].get_pos();
		auto const terminal_pos = graph[terminal_node].get_pos();

		graph[edge_key].set_location(initial_pos, terminal_pos);
	}
}
