#include "Entity/Mob/Player/Player.hpp"

#include <glm/gtx/rotate_vector.hpp>


Player::Player(PhysicsManager* const physics_manager) noexcept : Mob(Kind::player_entity, physics_manager), m_camera(*this) {}


void
Player::tick() noexcept {
	auto constexpr margin = 0.2F;
	auto constexpr max_pitch = 90.0F * (1.0F - margin);

	auto* physics = dynamic_cast<AdvancedPhysicsProperties*>(get_physics());
	assert(physics != nullptr);
	physics->reduce_torque();

	auto const facing = physics->get_facing();
	auto const angle = glm::degrees(std::asin(facing.y));
	auto const angle_magnitude = std::abs(angle);

	if (angle_magnitude < max_pitch) {
		return;
	}

	auto amt = max_pitch - angle_magnitude;
	if (angle < 0.0F) {
		amt *= -1.0F;
	}

	physics->set_facing(glm::rotate(facing, glm::radians(amt), physics->get_right()));
}


PlayerCamera const&
Player::get_camera() const noexcept {
	return m_camera;
}


PlayerCamera&
Player::get_camera() noexcept {
	return m_camera;
}
