#include "Entity/Mob/Player/PlayerCamera.hpp"

#include "Client/Client.hpp"

#include <glm/gtc/type_ptr.hpp>


namespace {
	auto constexpr UP = glm::vec3(0.0F, 1.0F, 0.0F);
}  // namespace


PlayerCamera::PlayerCamera(Player const& player) noexcept : m_player(player) {}


void
PlayerCamera::update(Renderer* const renderer) noexcept {
	renderer->get_shader().set_mat4("view", get_view());
}


glm::mat4
PlayerCamera::get_view() noexcept {
	return glm::lookAt(get_eye(), get_center(), get_up());
}

glm::vec3
PlayerCamera::get_eye() const noexcept {
	return get_properties().get_pos();
}


glm::vec3
PlayerCamera::get_center() const noexcept {
	return get_eye() + get_forward();
}


glm::vec3
PlayerCamera::get_up() noexcept {
	return UP;
}


glm::vec3
PlayerCamera::get_forward() const noexcept {
	return get_properties().get_facing();
}


AdvancedPhysicsProperties const&
PlayerCamera::get_properties() const noexcept {
	assert(m_player.get_physics_kind() == PhysicsProperties::Kind::adv_props);
	auto const* const property_ptr = dynamic_cast<AdvancedPhysicsProperties const* const>(m_player.get_physics());
	assert(property_ptr != nullptr);

	return *property_ptr;
}
