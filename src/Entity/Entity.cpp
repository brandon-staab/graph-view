#include "Entity/Entity.hpp"


Entity::Entity(Kind const kind) noexcept : m_kind(kind) {}


Entity::Kind
Entity::get_kind() const noexcept {
	return m_kind;
}


glm::mat4
Entity::get_matrix() const noexcept {
	return glm::mat4(1.0F);
}
